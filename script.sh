#!/bin/bash

DAT=`date +%d%m%Y%H%M%S`
LOG=/opt/logs/mylog_$DAT.log
DB_Name=mydb
CDR_PATH="/opt/rel"
CDR_FILE="GG_A_CDR.txt"

dest()
{
	case $1 in
		"SG" ) echo "Dest is OCS" >> $LOG
			Dest=OCS
			;;
		"GB" ) echo "Dest is OCB" >> $LOG
			Dest=OCB
			;;
		"IN" ) echo "Dest is OCIB" >> $LOG
			Dest=OCIB
			;;
		"*" ) echo "Unknown Dest"
			;;
	esac
}
#Add SQL function here

FILES=`find $CDR_PATH -type f -name $CDR_FILE`
echo "Below are the GG_A_CDR files" >> $LOG
echo "$FILES" >> $LOG

grep "GGReplicaiton" $FILES 1>/dev/null
if [ $? -eq 0 ]
then
	echo "We have GGReplicaiton occurrence in some files, proceeding with flow"
else
	echo "We dont have GGReplicaiton occurrence in some files, so stopping the process" >> $LOG
	echo "We dont have GGReplicaiton occurrence in some files, so stopping the process"
	exit 0
fi

for fil in `echo $FILES`
do
echo "Checking in $fil file" >> $LOG
grep "GGReplicaiton" $fil 1>/dev/null
if [ $? -eq 0 ]
then
	echo "$fil file is having GGReplicaiton value" >> $LOG
#else
#	echo "$fil file is not having GGReplicaiton value" >> $LOG
#fi
while  IFS= read -r line
do
	echo "$line" | grep "GGReplicaiton" 1>/dev/null
	if [ $? -eq 0 ]
	then
		echo "$line" >> $LOG
		WCOUNT=`echo "$line" | awk -F ";" '{print NF}'`
		echo "Word count: $WCOUNT" >> $LOG
		VAR1=`echo $line | cut -d";" -f1`
		S_Name=`echo $VAR1 | cut -d"." -f1`
		T_Name=`echo $VAR1 | cut -d"." -f2`
		Env_Name=`echo $S_Name | cut -d"_" -f1`
		#updated code from here
		if [[ "QAITNN" =~ "$Env_Name" || "CERT" =~ "$Env_Name" ]]
		then
			echo "ENV is CERT"
			Env_Name="CERT"
		else
			Env_Name=$Env_Name
			echo "ENV is $Env_Name"
		fi
		#updated up to here
		CDR_Key=`echo $line | cut -d";" -f3 | cut -d"=" -f2`
		VAR2=`echo $line | cut -d";" -f4 | cut -d"=" -f1 | tr -d " "`
		case $WCOUNT in
			"5" ) echo "We have 5 fields" >> $LOG
				if [ "$VAR2" = "AP" ]
				then
					echo "R_Type AP so vale is RAP" >> $LOG
					R_Type=RAP
				elif [ "$VAR2" = "AA" ]
				then
					echo "R_Type AA so vale is RAA" >> $LOG
					R_Type=RAA
				else
					echo "we can handle AP and AA" >> $LOG
				fi
				VAR3=`echo $line | cut -d";" -f4-`
				DEST1=`echo $VAR3 | cut -d"," -f2 | cut -d")" -f1`
				dest $DEST1
				QURY1=`echo "$S_Name,$T_Name,$DB_Name,$Env_Name,$CDR_Key,$R_Type,$Dest"`
				#To add single quotes to each variable value in QUERY add sed "s/\([^,]*\)/\'&\'/g" 
				echo "$QURY1" >> $LOG
				echo "$QURY1"
				add_table $QURY1
				DEST2=`echo $VAR3 | cut -d"," -f3 | cut -d")" -f1`
				dest $DEST2
				QURY2=`echo "$S_Name,$T_Name,$DB_Name,$Env_Name,$CDR_Key,$R_Type,$Dest"`
				echo "$QURY2" >> $LOG
				echo $QURY2
				add_table $QURY2
				;;
			"4" ) echo "We have 4 fields" >> $LOG
				R_Type=GAP
				Dest=REPORT
				QURY=`echo "$S_Name,$T_Name,$DB_Name,$Env_Name,$CDR_Key,$R_Type,$Dest"`
				echo "$QURY" >> $LOG
				echo $QURY
				add_table $QURY
				;;
			"3" ) echo "We have 3 fields" >> $LOG
				R_Type=GAA
				Dest=US
				QURY=`echo "$S_Name,$T_Name,$DB_Name,$Env_Name,$CDR_Key,$R_Type,$Dest"`
                                echo "$QURY" >> $LOG
				echo $QURY
				add_table $QURY
				;;
			"*" ) echo "Fields count is less than 3" >> $LOG
				echo "Fields count is less than 3"
				;;
		esac


	fi
done < $fil
else
	echo "$fil file is not having GGReplicaiton value" >> $LOG
fi
done

#sending mail
mail -s 'Subject-Here' mail@mailId.com < $LOG
