#!/bin/bash
DAT=`date +%d%m%Y%H%M%S`
LOG=/opt/logs/myDBlog_$DAT.log

dest()
{
	case $1 in
		"SG" ) echo "Dest is OCS" >> $LOG
			Dest=OCS
			;;
		"GB" ) echo "Dest is OCB" >> $LOG
			Dest=OCB
			;;
		"IN" ) echo "Dest is OCIB" >> $LOG
			Dest=OCIB
			;;
		"*" ) echo "Unknown Dest"
			;;
	esac
}


#checking for schemas in which we have 2 tables
echo "checking Schemas in which we have IN_COUNTRY_REPLICATION and BETWEEN_COUNTRY_REPLICATION" > $LOG
echo "checking Schemas in which we have IN_COUNTRY_REPLICATION and BETWEEN_COUNTRY_REPLICATION"
echo "spool schemas.txt;
select distinct owner from dba_tables where owner in (select username from all_users where oracle_maintained = 'N') and table_name in ('IN_COUNTRY_REPLICATION','BETWEEN_COUNTRY_REPLICATION') order by 1;
spool off;" > schemas.sql

#Running schemas.sql file in SQL DB
sqlplus -s username/password@servername @schemas.sql

#Checking if we found any schemas that contains IN_COUNTRY_REPLICATION and BETWEEN_COUNTRY_REPLICATION tables
if [ -s schemas.txt ]
then
	echo "schemas.txt is non empty means we have found some schemas with those tables" >> $LOG
	cat schemas.txt | grep -v 'schema\|-\|row' | sed '/^$/d' | sed 's/^ //g' > schemas.txt
	echo "Founded Schemas are:" >> $LOG
	cat schemas.txt >> $LOG
	echo "Founded Schemas are:"
	cat schemas.txt

#Looping through schemas which are found from above query 
	for i in `cat schemas.txt`
	do
		echo "Checking in $i schema " >> $LOG
		echo "Checking in $i schema "
		
#Looping two tables in schemas.
		for m in IN_COUNTRY_REPLICATIONS BETWEEN_COUNTRY_REPLICATION
		do
			echo "In $i schema checking in $m table" >> $LOG
			echo "spool table_info.txt;
			select * from $i.$m;
			spool off;" > table_info.sql
			sqlplus -s username/password@servername @table_info.sql
			
			#checking if we the IN_COUNTRY_REPLICATION or BETWEEN_COUNTRY_REPLICATION table is existing or not
			if [  -s table_info.txt ]
			then
				TABLES=`cat table_info.txt | grep -v 'UNIQUE_KEY_COLS\|-\|row' | sed '/^$/d' | awk -F"|" '{print $1}'| sed 's/^ //g'`
				
				echo "$TABLES are the tables present in $i schema $m table" >> $LOG
				
				for j in `cat table_info.txt | grep -v 'UNIQUE_KEY_COLS\|-\|row' | sed '/^$/d' | awk -F"|" '{print $1}'| sed 's/^ //g'`
			    do
					echo "Checking if $j table is exist or not " >> $LOG
					 
					echo "spool check_table.txt;
					select table_name from all_tables where owner='$i' and table_name='$j';
					spool off;" > check_table.sql
					sqlplus -s username/password@servername @check_table.sql
					
					#checking if the table is present or not.
					cat check_table.txt | grep $j
					if [ $? -eq 0 ]
					then
							echo "Table $j is exist" >> $LOG
					else
							echo "Table $j is not exist in $i schema" >> $LOG
							
					fi
					
					CDR_COL=`cat table_info.txt | grep $j | awk -F"|" '{print $5}' | sed 's/^ //g'`
					if [ -z $CDR_COL ]
					then
						echo "CDR_COL value is empty" >> $LOG
					else
						echo "CDR_COL value is $CDR_COL , present in table" >> $LOG
						
						echo "spool cdr_col.txt;
						select column_name from all_tab_columns where owner='$i' and table_name='$j' and column_nam='$CDR_COL';
						spool off;" > cdr_col.sql
						sqlplus -s username/password@servername @cdr_col.sql
						
						cat cdr_col.txt | grep $CDR_COL
						if [ $? -eq 0 ]
						then
							echo "$CDR_COL cdr_col is present in $j table of $i schema" >> $LOG 
						else
							echo "$CDR_COL cdr_col is not present in $j table of $i schema" >> $LOG
							continue
						fi
					fi
					
					UNIQ_KEY=`cat table_info.txt | grep $j | awk -F"|" '{print $4}' | sed 's/^ //g'`
					
					UNIQ_KEY=`echo $UNIQ_KEY | sed 's/,//g'`
					
					echo "For $j table in $i schema found $UNIQ_KEY uniq_key values" >> $LOG
					for k in $UNIQ_KEY
					do
						echo "checking the Uniq Key $k in table $j" >> $LOG
						 
						echo "spool uniq_key.txt;
						select column_name from all_tab_columns where owner='$i' and table_name='$j' and column_nam='$k';
						spool off;" > uniq_key.sql
						sqlplus -s username/password@servername @uniq_key.sql
						
						cat uniq_key.txt | grep $k
						if [ $? -eq 0 ]
						then
								echo "$k Uniq Key is present in $j table of $i schema" >> $LOG
								VAR2=`cat table_info.txt | grep -v 'REP\|-\|row' | grep $j | awk -F"|" '{print $3}' | sed 's/^ //g'`
								if [ "$VAR2" = "AP" ]
								then
									echo "R_Type AP so vale is RAP" >> $LOG
									R_Type=RAP
								elif [ "$VAR2" = "AA" ]
								then
									echo "R_Type AA so vale is RAA" >> $LOG
									R_Type=RAA
								else
									echo "we can handle AP and AA" >> $LOG
								fi
								VAR3=`cat table_info.txt | grep -v 'REP\|-\|row' | grep $j | awk -F"|" '{print $2}' | sed 's/^ //g'`
								dest $VAR3
								
								echo "Final values are :"
								echo "schemas : $i, Table name: $j, DB Name: Database, RType: $R_Type, Dest : $Dest "
						else
								echo "$k Uniq Key is not present in $j table of $i schema" >> $LOG
								continue
						fi
					done
				
				done
			else
				echo "table_info.txt file is empty means $m table not present in $i schema" >> $LOG
				continue
			fi
		done
		#Wrtie echo output here.
		
	done
else
	echo "schemas.txt file is empty means no schemas have been found with those tables" >> $LOG
fi

rm -rf schemas.txt schemas.sql table_info.txt table_info.sql check_table.txt check_table.sql cdr_col.txt cdr_col.sql uniq_key.txt uniq_key.sql 
